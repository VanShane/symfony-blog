/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import './styles/global.scss';

import  './images/banniere.jpg';
import './images/banner.jpg';
import  './images/article.jpg';

// start the Stimulus application
// import './bootstrap';

const $ = require('jquery');
require('bootstrap');
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all.js';
