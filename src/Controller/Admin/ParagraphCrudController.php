<?php

namespace App\Controller\Admin;

use App\Entity\Paragraph;
use App\Entity\ParagraphPicture;
use App\Form\ParagraphPictureType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ParagraphCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Paragraph::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('article'),
            TextField::new('title'),
            TextField::new('slug')->hideOnForm(),
            TextEditorField::new('content'),
            DateTimeField::new('updated_at')->hideOnForm(),
            CollectionField::new('paragraphPictures')
                ->setEntryType(ParagraphPictureType::class)
                ->setFormTypeOption('by_reference', false)
                ->onlyOnForms(),
            CollectionField::new('paragraphPictures')
                ->setTemplatePath('admin/paragraph_pictures.html.twig')
                ->hideOnForm(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
    
}
