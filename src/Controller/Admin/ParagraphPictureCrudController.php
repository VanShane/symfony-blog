<?php

namespace App\Controller\Admin;

use App\Entity\ParagraphPicture;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ParagraphPictureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ParagraphPicture::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('paragraphs')->hideOnForm(),
            ImageField::new('picture')->hideOnForm()->setBasePath('/images/paragraph-pictures'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->disable('new', 'edit', 'delete');
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);
        return $actions;
    }
    
}
