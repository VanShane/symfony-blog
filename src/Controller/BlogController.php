<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="blog_")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("", name="index")
     *
     * @param ArticleRepository $articleRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index(ArticleRepository $articleRepository, CategoryRepository $categoryRepository): Response
    {
        $articles = $articleRepository->findAll();

        $categories = $categoryRepository->findAll();

        $url_slug = 'all';

        return $this->render('blog/index.html.twig', [
            'articles' => $articles,
            'categories' => $categories,
            'url_slug' => $url_slug,
        ]);
    }

    /**
     * @Route("/category/{slug}", name="category")
     *
     * @param Category $category
     * @param CategoryRepository $categoryRepository
     * @param string $slug
     * @return Response
     */
    public function byCategory(Category $category, CategoryRepository $categoryRepository, string $slug, Request $request): Response
    {
        $articles = $category->getArticles();

        $categories = $categoryRepository->findAll();

        $url_slug = $request->attributes->get('slug');

        return $this->render('blog/index.html.twig', [
            'articles' => $articles,
            'categories' => $categories,
            'url_slug' => $url_slug,
        ]);
    }

    /**
     * @Route("/show/{slug}", name="show")
     *
     * @param ArticleRepository $articleRepository
     * @param string $slug
     * @param Article $article
     * @return Response
     */
    public function show(ArticleRepository $articleRepository, string $slug, Article $article): Response
    {
        $slug = $article->getSlug();
        $articles = $articleRepository->findOneBy(['slug' => $slug]);

        return $this->render('blog/show.html.twig', [
            'article' => $article,
        ]);
    }
}
