<?php

namespace App\Entity;

use App\Repository\ParagraphPictureRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ParagraphPictureRepository::class)
 * @Vich\Uploadable()
 */
class ParagraphPicture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @Vich\UploadableField(mapping="paragraph_pictures", fileNameProperty="picture")
     */
    private $pictureFile;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Paragraph::class, inversedBy="paragraphPictures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $paragraphs;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getParagraphs(): ?Paragraph
    {
        return $this->paragraphs;
    }

    public function setParagraphs(?Paragraph $paragraphs): self
    {
        $this->paragraphs = $paragraphs;

        return $this;
    }

    /**
     * Get the value of pictureFile
     */ 
    public function getPictureFile()
    {
        return $this->pictureFile;
    }

    /**
     * Set the value of pictureFile
     *
     * @return  self
     */ 
    public function setPictureFile($pictureFile)
    {
        $this->pictureFile = $pictureFile;

        if($pictureFile) {
            $this->updatedAt = new \DateTime();
        }

        return $this;
    }
}
