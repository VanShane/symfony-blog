<?php

namespace App\Entity;

use App\Repository\ParagraphRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=ParagraphRepository::class)
 */
class Paragraph
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="paragraphs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @ORM\OneToMany(targetEntity=ParagraphPicture::class, mappedBy="paragraphs", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $paragraphPictures;

    public function __construct()
    {
        $this->blogPictures = new ArrayCollection();
        $this->paragraphPictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString(): ?string
    {
        return $this->slug;
    }

    /**
     * @return Collection|ParagraphPicture[]
     */
    public function getParagraphPictures(): Collection
    {
        return $this->paragraphPictures;
    }

    public function addParagraphPicture(ParagraphPicture $paragraphPicture): self
    {
        if (!$this->paragraphPictures->contains($paragraphPicture)) {
            $this->paragraphPictures[] = $paragraphPicture;
            $paragraphPicture->setParagraphs($this);
        }

        return $this;
    }

    public function removeParagraphPicture(ParagraphPicture $paragraphPicture): self
    {
        if ($this->paragraphPictures->removeElement($paragraphPicture)) {
            // set the owning side to null (unless already changed)
            if ($paragraphPicture->getParagraphs() === $this) {
                $paragraphPicture->setParagraphs(null);
            }
        }

        return $this;
    }
}
