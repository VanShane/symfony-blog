<?php

namespace App\Repository;

use App\Entity\ParagraphPicture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParagraphPicture|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParagraphPicture|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParagraphPicture[]    findAll()
 * @method ParagraphPicture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParagraphPictureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParagraphPicture::class);
    }

    // /**
    //  * @return ParagraphPicture[] Returns an array of ParagraphPicture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParagraphPicture
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
